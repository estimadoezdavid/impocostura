/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class TipoDocumento {

    //declaro variables de la entidad Tipo de documento
    private int idTipoDocumento;
    private String descripcion;

    //constructor por defecto
    public TipoDocumento() {
    }

    //construcctor sobrecargado
    public TipoDocumento(int idTipoDocumento, String descripcion) {
        this.idTipoDocumento = idTipoDocumento;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}