/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

import java.sql.Date;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class Cliente {

    //declaro variables de la entidad Cliente
    private int idCliente;
    private long identificacion;
    private String nombres;
    private String apellidos;
    private Date fechaNacimiento;
    private String direccion;
    private Genero idGenero;
    private TipoDocumento idTipoDocumento;

    //constructor por defecto
    public Cliente() {
    }

    //construcctor sobrecargado
    public Cliente(int idCliente, long identificacion, String nombres, String apellidos, Date fechaNacimiento, String direccion, Genero idGenero, TipoDocumento idTipoDocumento) {
        this.idCliente = idCliente;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.idGenero = idGenero;
        this.idTipoDocumento = idTipoDocumento;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Genero getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(Genero idGenero) {
        this.idGenero = idGenero;
    }

    public TipoDocumento getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TipoDocumento idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

}