/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class DetalleVenta {

    //declaro variables de la entidad Detalle de venta
    private int idDetalleVenta;
    private short cantidad;
    private byte garantia;
    private PrecioVenta idPrecioVenta;
    private Factura idFactura;
    private Cliente idCliente;

    //constructor por defecto
    public DetalleVenta() {
    }

    //construcctor sobrecargado
    public DetalleVenta(int idDetalleVenta, short cantidad, byte garantia, PrecioVenta idPrecioVenta, Factura idFactura, Cliente idCliente) {
        this.idDetalleVenta = idDetalleVenta;
        this.cantidad = cantidad;
        this.garantia = garantia;
        this.idPrecioVenta = idPrecioVenta;
        this.idFactura = idFactura;
        this.idCliente = idCliente;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdDetalleVenta() {
        return idDetalleVenta;
    }

    public void setIdDetalleVenta(int idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public byte getGarantia() {
        return garantia;
    }

    public void setGarantia(byte garantia) {
        this.garantia = garantia;
    }

    public PrecioVenta getIdPrecioVenta() {
        return idPrecioVenta;
    }

    public void setIdPrecioVenta(PrecioVenta idPrecioVenta) {
        this.idPrecioVenta = idPrecioVenta;
    }

    public Factura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Factura idFactura) {
        this.idFactura = idFactura;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

}