/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

import java.sql.Timestamp;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class Factura {

    //declaro variables de la entidad Factura
    private int idFactura;
    private String consecutivo;
    private Timestamp fechaYHora;
    private String descripcion;
    private int total;
    private MetodoPago idMetodoPago;

    //constructor por defecto
    public Factura() {
    }

    //construcctor sobrecargado
    public Factura(int idFactura, String consecutivo, Timestamp fechaYHora, String descripcion, int total, MetodoPago idMetodoPago) {
        this.idFactura = idFactura;
        this.consecutivo = consecutivo;
        this.fechaYHora = fechaYHora;
        this.descripcion = descripcion;
        this.total = total;
        this.idMetodoPago = idMetodoPago;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public Timestamp getFechaYHora() {
        return fechaYHora;
    }

    public void setFechaYHora(Timestamp fechaYHora) {
        this.fechaYHora = fechaYHora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public MetodoPago getIdMetodoPago() {
        return idMetodoPago;
    }

    public void setIdMetodoPago(MetodoPago idMetodoPago) {
        this.idMetodoPago = idMetodoPago;
    }

}