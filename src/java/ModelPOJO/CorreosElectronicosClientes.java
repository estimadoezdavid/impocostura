/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class CorreosElectronicosClientes {

    //declaro variables de la entidad Correos electrónicos de clientes
    private int idCorreosElectronicosClientes;
    private String descripcion;
    private Cliente idCliente;

    //constructor por defecto
    public CorreosElectronicosClientes() {
    }

    //construcctor sobrecargado
    public CorreosElectronicosClientes(int idCorreosElectronicosClientes, String descripcion, Cliente idCliente) {
        this.idCorreosElectronicosClientes = idCorreosElectronicosClientes;
        this.descripcion = descripcion;
        this.idCliente = idCliente;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdCorreosElectronicosClientes() {
        return idCorreosElectronicosClientes;
    }

    public void setIdCorreosElectronicosClientes(int idCorreosElectronicosClientes) {
        this.idCorreosElectronicosClientes = idCorreosElectronicosClientes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

}