/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class MetodoPago {

    //declaro variables de la entidad Metodo de pago
    private int idMetodoPago;
    private String descripcion;
    private String detalles;

    //constructor por defecto
    public MetodoPago() {
    }

    //construcctor sobrecargado
    public MetodoPago(int idMetodoPago, String descripcion, String detalles) {
        this.idMetodoPago = idMetodoPago;
        this.descripcion = descripcion;
        this.detalles = detalles;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdMetodoPago() {
        return idMetodoPago;
    }

    public void setIdMetodoPago(int idMetodoPago) {
        this.idMetodoPago = idMetodoPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

}