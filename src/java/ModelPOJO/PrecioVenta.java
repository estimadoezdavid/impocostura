/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class PrecioVenta {

    //declaro variables de la entidad Precio de venta
    private int idPrecioVenta;
    private int valorUnidad;
    private Producto idProducto;
    private TipoCliente idTipoCliente;

    //constructor por defecto
    public PrecioVenta() {
    }

    //construcctor sobrecargado
    public PrecioVenta(int idPrecioVenta, int valorUnidad, Producto idProducto, TipoCliente idTipoCliente) {
        this.idPrecioVenta = idPrecioVenta;
        this.valorUnidad = valorUnidad;
        this.idProducto = idProducto;
        this.idTipoCliente = idTipoCliente;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdPrecioVenta() {
        return idPrecioVenta;
    }

    public void setIdPrecioVenta(int idPrecioVenta) {
        this.idPrecioVenta = idPrecioVenta;
    }

    public int getValorUnidad() {
        return valorUnidad;
    }

    public void setValorUnidad(int valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public TipoCliente getIdTipoCliente() {
        return idTipoCliente;
    }

    public void setIdTipoCliente(TipoCliente idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

}