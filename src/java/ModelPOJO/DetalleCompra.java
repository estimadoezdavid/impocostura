/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class DetalleCompra {

    //declaro variables de la entidad Detalle de compra
    private int idDetalleCompra;
    private short cantidad;
    private int valorUnidad;
    private byte garantia;
    private Producto idProducto;
    private Proveedor idProveedor;
    private Factura idFactura;

    //constructor por defecto
    public DetalleCompra() {
    }

    //construcctor sobrecargado
    public DetalleCompra(int idDetalleCompra, short cantidad, int valorUnidad, byte garantia, Producto idProducto, Proveedor idProveedor, Factura idFactura) {
        this.idDetalleCompra = idDetalleCompra;
        this.cantidad = cantidad;
        this.valorUnidad = valorUnidad;
        this.garantia = garantia;
        this.idProducto = idProducto;
        this.idProveedor = idProveedor;
        this.idFactura = idFactura;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdDetalleCompra() {
        return idDetalleCompra;
    }

    public void setIdDetalleCompra(int idDetalleCompra) {
        this.idDetalleCompra = idDetalleCompra;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public int getValorUnidad() {
        return valorUnidad;
    }

    public void setValorUnidad(int valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    public byte getGarantia() {
        return garantia;
    }

    public void setGarantia(byte garantia) {
        this.garantia = garantia;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Proveedor getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Proveedor idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Factura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Factura idFactura) {
        this.idFactura = idFactura;
    }

}