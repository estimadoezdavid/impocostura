/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class TipoCliente {

    //declaro variables de la entidad Tipo de cliente
    private int idTipoCliente;
    private String descripcion;

    //constructor por defecto
    public TipoCliente() {
    }

    //construcctor sobrecargado
    public TipoCliente(int idTipoCliente, String descripcion) {
        this.idTipoCliente = idTipoCliente;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdTipoCliente() {
        return idTipoCliente;
    }

    public void setIdTipoCliente(int idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}