/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class NumerosContactoProveedores {

    //declaro variables de la entidad Numeros de contacto de proveedores
    private int idNumerosContactoProveedores;
    private String descripcion;
    private Proveedor idProveedor;
    private TipoNumeroContacto idTipoNumeroContacto;

    //constructor por defecto
    public NumerosContactoProveedores() {
    }

    //construcctor sobrecargado
    public NumerosContactoProveedores(int idNumerosContactoProveedores, String descripcion, Proveedor idProveedor, TipoNumeroContacto idTipoNumeroContacto) {
        this.idNumerosContactoProveedores = idNumerosContactoProveedores;
        this.descripcion = descripcion;
        this.idProveedor = idProveedor;
        this.idTipoNumeroContacto = idTipoNumeroContacto;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdNumerosContactoProveedores() {
        return idNumerosContactoProveedores;
    }

    public void setIdNumerosContactoProveedores(int idNumerosContactoProveedores) {
        this.idNumerosContactoProveedores = idNumerosContactoProveedores;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proveedor getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Proveedor idProveedor) {
        this.idProveedor = idProveedor;
    }

    public TipoNumeroContacto getIdTipoNumeroContacto() {
        return idTipoNumeroContacto;
    }

    public void setIdTipoNumeroContacto(TipoNumeroContacto idTipoNumeroContacto) {
        this.idTipoNumeroContacto = idTipoNumeroContacto;
    }

}