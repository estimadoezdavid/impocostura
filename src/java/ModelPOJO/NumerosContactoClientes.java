/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class NumerosContactoClientes {

    //declaro variables de la entidad Números de contacto de clientes
    private int idNumerosContactoClientes;
    private String descripcion;
    private Cliente idCliente;
    private TipoNumeroContacto idTipoNumeroContacto;

    //constructor por defecto
    public NumerosContactoClientes() {
    }

    //construcctor sobrecargado
    public NumerosContactoClientes(int idNumerosContactoClientes, String descripcion, Cliente idCliente, TipoNumeroContacto idTipoNumeroContacto) {
        this.idNumerosContactoClientes = idNumerosContactoClientes;
        this.descripcion = descripcion;
        this.idCliente = idCliente;
        this.idTipoNumeroContacto = idTipoNumeroContacto;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdNumerosContactoClientes() {
        return idNumerosContactoClientes;
    }

    public void setIdNumerosContactoClientes(int idNumerosContactoClientes) {
        this.idNumerosContactoClientes = idNumerosContactoClientes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public TipoNumeroContacto getIdTipoNumeroContacto() {
        return idTipoNumeroContacto;
    }

    public void setIdTipoNumeroContacto(TipoNumeroContacto idTipoNumeroContacto) {
        this.idTipoNumeroContacto = idTipoNumeroContacto;
    }

}