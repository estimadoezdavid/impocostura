/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class CorreosElectronicosProveedores {

    //declaro variables de la entidad Correos electrónicos de proveedores
    private int idCorreosElectronicosProveedores;
    private String descripcion;
    private Proveedor idProveedor;

    //constructor por defecto
    public CorreosElectronicosProveedores() {
    }

    //construcctor sobrecargado
    public CorreosElectronicosProveedores(int idCorreosElectronicosProveedores, String descripcion, Proveedor idProveedor) {
        this.idCorreosElectronicosProveedores = idCorreosElectronicosProveedores;
        this.descripcion = descripcion;
        this.idProveedor = idProveedor;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdCorreosElectronicosProveedores() {
        return idCorreosElectronicosProveedores;
    }

    public void setIdCorreosElectronicosProveedores(int idCorreosElectronicosProveedores) {
        this.idCorreosElectronicosProveedores = idCorreosElectronicosProveedores;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Proveedor getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Proveedor idProveedor) {
        this.idProveedor = idProveedor;
    }

}