/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class TipoNumeroContacto {

    //declaro variables de la entidad Tipo de número de contacto
    private int idTipoNumeroContacto;
    private String descripcion;

    //constructor por defecto
    public TipoNumeroContacto() {
    }

    //construcctor sobrecargado
    public TipoNumeroContacto(int idTipoNumeroContacto, String descripcion) {
        this.idTipoNumeroContacto = idTipoNumeroContacto;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdTipoNumeroContacto() {
        return idTipoNumeroContacto;
    }

    public void setIdTipoNumeroContacto(int idTipoNumeroContacto) {
        this.idTipoNumeroContacto = idTipoNumeroContacto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}