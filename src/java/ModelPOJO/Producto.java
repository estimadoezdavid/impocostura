/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class Producto {

    //declaro variables de la entidad Producto
    private int idProducto;
    private String codigo;
    private String serial;
    private String nombre;
    private int stock;
    private String descripcion;
    private String image;
    private TipoProducto idTipoProducto;
    private ClaseProducto idClaseProducto;
    private Marca idMarca;
    private EstadoProducto idEstadoProducto;

    //constructor por defecto
    public Producto() {
    }

    //construcctor sobrecargado
    public Producto(int idProducto, String codigo, String serial, String nombre, int stock, String descripcion, String image, TipoProducto idTipoProducto, ClaseProducto idClaseProducto, Marca idMarca, EstadoProducto idEstadoProducto) {
        this.idProducto = idProducto;
        this.codigo = codigo;
        this.serial = serial;
        this.nombre = nombre;
        this.stock = stock;
        this.descripcion = descripcion;
        this.image = image;
        this.idTipoProducto = idTipoProducto;
        this.idClaseProducto = idClaseProducto;
        this.idMarca = idMarca;
        this.idEstadoProducto = idEstadoProducto;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public TipoProducto getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(TipoProducto idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public ClaseProducto getIdClaseProducto() {
        return idClaseProducto;
    }

    public void setIdClaseProducto(ClaseProducto idClaseProducto) {
        this.idClaseProducto = idClaseProducto;
    }

    public Marca getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Marca idMarca) {
        this.idMarca = idMarca;
    }

    public EstadoProducto getIdEstadoProducto() {
        return idEstadoProducto;
    }

    public void setIdEstadoProducto(EstadoProducto idEstadoProducto) {
        this.idEstadoProducto = idEstadoProducto;
    }

}