/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class EstadoProducto {

    //declaro variables de la entidad Estado de producto
    private int idEstadoProducto;
    private String descripcion;

    //constructor por defecto
    public EstadoProducto() {
    }

    //construcctor sobrecargado
    public EstadoProducto(int idEstadoProducto, String descripcion) {
        this.idEstadoProducto = idEstadoProducto;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdEstadoProducto() {
        return idEstadoProducto;
    }

    public void setIdEstadoProducto(int idEstadoProducto) {
        this.idEstadoProducto = idEstadoProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}