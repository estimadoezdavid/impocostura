/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class Proveedor {

    //declaro variables de la entidad Proveedor
    private int idProveedor;
    private long identificacion;
    private String razonSocial;
    private String direccion;
    private String representante;
    private TipoDocumento idTipoDocumento;

    //constructor por defecto
    public Proveedor() {
    }

    //construcctor sobrecargado
    public Proveedor(int idProveedor, long identificacion, String razonSocial, String direccion, String representante, TipoDocumento idTipoDocumento) {
        this.idProveedor = idProveedor;
        this.identificacion = identificacion;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.representante = representante;
        this.idTipoDocumento = idTipoDocumento;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    public long getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public TipoDocumento getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TipoDocumento idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

}