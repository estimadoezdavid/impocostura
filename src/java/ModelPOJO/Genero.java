/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class Genero {

    //declaro variables de la entidad Género
    private int idGenero;
    private String descripcion;

    //constructor por defecto
    public Genero() {
    }

    //constructor sobrecargado
    public Genero(int idGenero, String descripcion) {
        this.idGenero = idGenero;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
