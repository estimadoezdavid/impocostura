/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelPOJO;

/**
 *
 * @author User
 */
//clase pública usada en demás paquetes
public class ClaseProducto {

    //declaro variables de la entidad Clase del producto
    private int idClaseProducto;
    private String descripcion;

    //constructor por defecto
    public ClaseProducto() {
    }

    //construcctor sobrecargado
    public ClaseProducto(int idClaseProducto, String descripcion) {
        this.idClaseProducto = idClaseProducto;
        this.descripcion = descripcion;
    }

    //encapsulamiento mediante métodos getter and setter
    public int getIdClaseProducto() {
        return idClaseProducto;
    }

    public void setIdClaseProducto(int idClaseProducto) {
        this.idClaseProducto = idClaseProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}