-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-09-2020 a las 09:32:33
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `impocostura`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase_producto`
--

CREATE TABLE `clase_producto` (
  `IdClaseProducto` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `IdCliente` int(11) NOT NULL,
  `Identificacion` bigint(20) NOT NULL,
  `Nombres` varchar(50) NOT NULL,
  `Apellidos` varchar(50) NOT NULL,
  `Fecha de nacimiento` date NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `IdGenero` int(11) NOT NULL,
  `IdTipoDocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos_electronicos_clientes`
--

CREATE TABLE `correos_electronicos_clientes` (
  `IdCorreosElectronicosClientes` int(11) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `IdCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos_electronicos_proveedores`
--

CREATE TABLE `correos_electronicos_proveedores` (
  `IdCorreosElectronicosProveedores` int(11) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `IdProveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `IdDetalleCompra` int(11) NOT NULL,
  `Cantidad` smallint(6) NOT NULL,
  `Valor de unidad` int(11) NOT NULL,
  `Garantia` tinyint(4) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `IdProveedor` int(11) NOT NULL,
  `IdFactura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `IdDetalleVenta` int(11) NOT NULL,
  `Cantidad` smallint(6) NOT NULL,
  `Garantia` tinyint(4) NOT NULL,
  `IdPrecioVenta` int(11) NOT NULL,
  `IdFactura` int(11) NOT NULL,
  `IdCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_producto`
--

CREATE TABLE `estado_producto` (
  `IdEstadoProducto` int(11) NOT NULL,
  `Descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `IdFactura` int(11) NOT NULL,
  `Consecutivo` varchar(500) NOT NULL,
  `Fecha y hora` datetime NOT NULL,
  `Descripcion` varchar(500) NOT NULL,
  `Total` int(11) NOT NULL,
  `IdMetodoPago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `IdGenero` int(11) NOT NULL,
  `Descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `IdMarca` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodo_pago`
--

CREATE TABLE `metodo_pago` (
  `IdMetodoPago` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL,
  `Detalles` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numeros_contacto_clientes`
--

CREATE TABLE `numeros_contacto_clientes` (
  `IdNumerosContactoClientes` int(11) NOT NULL,
  `Descripcion` bigint(20) NOT NULL,
  `IdCliente` int(11) NOT NULL,
  `IdTipoNumeroContacto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numeros_contacto_proveedores`
--

CREATE TABLE `numeros_contacto_proveedores` (
  `IdNumerosContactoProveedores` int(11) NOT NULL,
  `Descripcion` bigint(20) NOT NULL,
  `IdProveedor` int(11) NOT NULL,
  `IdTipoNumeroContacto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_venta`
--

CREATE TABLE `precio_venta` (
  `IdPrecioVenta` int(11) NOT NULL,
  `Valor de unidad` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `IdTipoCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `IdProducto` int(11) NOT NULL,
  `Codigo` varchar(50) NOT NULL,
  `Serial` varchar(100) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Stock` int(11) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `Imagen` varchar(250) NOT NULL,
  `IdTipoProducto` int(11) NOT NULL,
  `IdClaseProducto` int(11) NOT NULL,
  `IdMarca` int(11) NOT NULL,
  `IdEstadoProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `IdProveedor` int(11) NOT NULL,
  `Identificacion` bigint(20) NOT NULL,
  `Razon social` varchar(100) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Representante` varchar(100) NOT NULL,
  `IdTipoDocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente`
--

CREATE TABLE `tipo_cliente` (
  `IdTipoCliente` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `IdTipoDocumento` int(11) NOT NULL,
  `Descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_numero_contacto`
--

CREATE TABLE `tipo_numero_contacto` (
  `IdTipoNumeroContacto` int(11) NOT NULL,
  `Descripcion` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `IdTipoProducto` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clase_producto`
--
ALTER TABLE `clase_producto`
  ADD PRIMARY KEY (`IdClaseProducto`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`IdCliente`),
  ADD UNIQUE KEY `Identificación del cliente` (`Identificacion`),
  ADD KEY `fk_cliente_genero` (`IdGenero`),
  ADD KEY `fk_cliente_tipo_documento` (`IdTipoDocumento`) USING BTREE;

--
-- Indices de la tabla `correos_electronicos_clientes`
--
ALTER TABLE `correos_electronicos_clientes`
  ADD PRIMARY KEY (`IdCorreosElectronicosClientes`),
  ADD KEY `fk_correos_electronicos_clientes_cliente` (`IdCliente`);

--
-- Indices de la tabla `correos_electronicos_proveedores`
--
ALTER TABLE `correos_electronicos_proveedores`
  ADD PRIMARY KEY (`IdCorreosElectronicosProveedores`),
  ADD KEY `fk_correos_electronicos_proveedores_proveedor` (`IdProveedor`);

--
-- Indices de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`IdDetalleCompra`),
  ADD KEY `fk_detalle_compra_producto` (`IdProducto`) USING BTREE,
  ADD KEY `fk_detalle_compra_proveedor` (`IdProveedor`) USING BTREE,
  ADD KEY `fk_detalle_compra_factura` (`IdFactura`) USING BTREE;

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`IdDetalleVenta`),
  ADD KEY `fk_detalle_venta_precio_venta` (`IdPrecioVenta`),
  ADD KEY `fk_detalle_venta_factura` (`IdFactura`),
  ADD KEY `fk_detalle_venta_cliente` (`IdCliente`);

--
-- Indices de la tabla `estado_producto`
--
ALTER TABLE `estado_producto`
  ADD PRIMARY KEY (`IdEstadoProducto`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`IdFactura`),
  ADD UNIQUE KEY `Consecutivo de la factura` (`Consecutivo`),
  ADD KEY `fk_factura_metodo_pago` (`IdMetodoPago`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`IdGenero`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`IdMarca`);

--
-- Indices de la tabla `metodo_pago`
--
ALTER TABLE `metodo_pago`
  ADD PRIMARY KEY (`IdMetodoPago`);

--
-- Indices de la tabla `numeros_contacto_clientes`
--
ALTER TABLE `numeros_contacto_clientes`
  ADD PRIMARY KEY (`IdNumerosContactoClientes`),
  ADD KEY `fk_numeros_contacto_clientes_cliente` (`IdCliente`),
  ADD KEY `fk_numeros_contacto_clientes_tipo_numero_contacto` (`IdTipoNumeroContacto`);

--
-- Indices de la tabla `numeros_contacto_proveedores`
--
ALTER TABLE `numeros_contacto_proveedores`
  ADD PRIMARY KEY (`IdNumerosContactoProveedores`),
  ADD KEY `fk_numeros_contacto_proveedores_proveedor` (`IdProveedor`),
  ADD KEY `fk_numeros_contacto_proveedores_tipo_numero_contacto` (`IdTipoNumeroContacto`);

--
-- Indices de la tabla `precio_venta`
--
ALTER TABLE `precio_venta`
  ADD PRIMARY KEY (`IdPrecioVenta`),
  ADD KEY `fk_precio_venta_producto` (`IdProducto`) USING BTREE,
  ADD KEY `fk_precio_venta_tipo_cliente` (`IdTipoCliente`) USING BTREE;

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`IdProducto`),
  ADD UNIQUE KEY `Código del producto` (`Codigo`),
  ADD KEY `fk_producto_tipo_producto` (`IdTipoProducto`),
  ADD KEY `fk_producto_marca` (`IdMarca`),
  ADD KEY `fk_producto_estado_producto` (`IdEstadoProducto`),
  ADD KEY `fk_producto_clase_producto` (`IdClaseProducto`) USING BTREE;

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`IdProveedor`),
  ADD UNIQUE KEY `Identificación del proveedor` (`Identificacion`),
  ADD KEY `fk_proveedor_tipo_documento` (`IdTipoDocumento`);

--
-- Indices de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  ADD PRIMARY KEY (`IdTipoCliente`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`IdTipoDocumento`);

--
-- Indices de la tabla `tipo_numero_contacto`
--
ALTER TABLE `tipo_numero_contacto`
  ADD PRIMARY KEY (`IdTipoNumeroContacto`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`IdTipoProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clase_producto`
--
ALTER TABLE `clase_producto`
  MODIFY `IdClaseProducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `correos_electronicos_clientes`
--
ALTER TABLE `correos_electronicos_clientes`
  MODIFY `IdCorreosElectronicosClientes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `correos_electronicos_proveedores`
--
ALTER TABLE `correos_electronicos_proveedores`
  MODIFY `IdCorreosElectronicosProveedores` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `IdDetalleCompra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `IdDetalleVenta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado_producto`
--
ALTER TABLE `estado_producto`
  MODIFY `IdEstadoProducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `IdFactura` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `IdGenero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `IdMarca` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `metodo_pago`
--
ALTER TABLE `metodo_pago`
  MODIFY `IdMetodoPago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `numeros_contacto_clientes`
--
ALTER TABLE `numeros_contacto_clientes`
  MODIFY `IdNumerosContactoClientes` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `numeros_contacto_proveedores`
--
ALTER TABLE `numeros_contacto_proveedores`
  MODIFY `IdNumerosContactoProveedores` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `precio_venta`
--
ALTER TABLE `precio_venta`
  MODIFY `IdPrecioVenta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `IdProducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `IdProveedor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente`
--
ALTER TABLE `tipo_cliente`
  MODIFY `IdTipoCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `IdTipoDocumento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_numero_contacto`
--
ALTER TABLE `tipo_numero_contacto`
  MODIFY `IdTipoNumeroContacto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `IdTipoProducto` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`IdTipoDocumento`) REFERENCES `tipo_documento` (`IdTipoDocumento`),
  ADD CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`IdGenero`) REFERENCES `genero` (`IdGenero`);

--
-- Filtros para la tabla `correos_electronicos_clientes`
--
ALTER TABLE `correos_electronicos_clientes`
  ADD CONSTRAINT `correos_electronicos_clientes_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`);

--
-- Filtros para la tabla `correos_electronicos_proveedores`
--
ALTER TABLE `correos_electronicos_proveedores`
  ADD CONSTRAINT `correos_electronicos_proveedores_ibfk_1` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`);

--
-- Filtros para la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD CONSTRAINT `detalle_compra_ibfk_1` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`IdProducto`),
  ADD CONSTRAINT `detalle_compra_ibfk_2` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`),
  ADD CONSTRAINT `detalle_compra_ibfk_3` FOREIGN KEY (`IdFactura`) REFERENCES `factura` (`IdFactura`);

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`IdPrecioVenta`) REFERENCES `precio_venta` (`IdPrecioVenta`),
  ADD CONSTRAINT `detalle_venta_ibfk_2` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  ADD CONSTRAINT `detalle_venta_ibfk_3` FOREIGN KEY (`IdFactura`) REFERENCES `factura` (`IdFactura`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`IdMetodoPago`) REFERENCES `metodo_pago` (`IdMetodoPago`);

--
-- Filtros para la tabla `numeros_contacto_clientes`
--
ALTER TABLE `numeros_contacto_clientes`
  ADD CONSTRAINT `numeros_contacto_clientes_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  ADD CONSTRAINT `numeros_contacto_clientes_ibfk_2` FOREIGN KEY (`IdTipoNumeroContacto`) REFERENCES `tipo_numero_contacto` (`IdTipoNumeroContacto`);

--
-- Filtros para la tabla `numeros_contacto_proveedores`
--
ALTER TABLE `numeros_contacto_proveedores`
  ADD CONSTRAINT `numeros_contacto_proveedores_ibfk_1` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`),
  ADD CONSTRAINT `numeros_contacto_proveedores_ibfk_2` FOREIGN KEY (`IdTipoNumeroContacto`) REFERENCES `tipo_numero_contacto` (`IdTipoNumeroContacto`);

--
-- Filtros para la tabla `precio_venta`
--
ALTER TABLE `precio_venta`
  ADD CONSTRAINT `precio_venta_ibfk_1` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`IdProducto`),
  ADD CONSTRAINT `precio_venta_ibfk_2` FOREIGN KEY (`IdTipoCliente`) REFERENCES `tipo_cliente` (`IdTipoCliente`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`IdTipoProducto`) REFERENCES `tipo_producto` (`IdTipoProducto`),
  ADD CONSTRAINT `producto_ibfk_2` FOREIGN KEY (`IdMarca`) REFERENCES `marca` (`IdMarca`),
  ADD CONSTRAINT `producto_ibfk_3` FOREIGN KEY (`IdEstadoProducto`) REFERENCES `estado_producto` (`IdEstadoProducto`),
  ADD CONSTRAINT `producto_ibfk_4` FOREIGN KEY (`IdClaseProducto`) REFERENCES `clase_producto` (`IdClaseProducto`);

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`IdTipoDocumento`) REFERENCES `tipo_documento` (`IdTipoDocumento`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
